﻿Imports INS_CS_Hist_Helper
Imports System.ComponentModel
Imports System.Reflection
Imports System.IO
Imports Proficy.iFixToolkit

    'Public NotInheritable Class ToolkitError
    '    Public Const BadHdaGroup As Short = 11002
    '    Public Const BadHdaNtf As Short = 11003
    '    Public Const BadRegistrationOrder As Short = 11016
    '    Public Const CantFindFix32Files As Short = 11029
    '    Public Const CantFindHistorian As Short = 11041
    '    Public Const ConsoleAppAlreadyReg As Short = 11024
    '    Public Const ConsoleAppCantReg As Short = 11025
    '    Public Const DataNotReadyYet As Short = 11023
    '    Public Const FixNotRunning As Short = 11018
    '    Public Const FixVersionIncompatible As Short = 11030
    '    Public Const HistorianCountError As Short = 11039
    '    Public Const HistorianErrorStart As Short = 11038
    '    Public Const HistorianServerError As Short = 11037
    '    Public Const HistorianValueError As Short = 11040
    '    Public Const IndexOutOfRange As Short = 11006
    '    Public Const InternalError As Short = 11028
    '    Public Const InvalidDate As Short = 11004
    '    Public Const InvalidNodeName As Short = 11022
    '    Public Const InvalidNtfFormat As Short = 11008
    '    Public Const InvalidPath As Short = 11020
    '    Public Const InvalidPointer As Short = 11011
    '    Public Const InvalidProgramName As Short = 11019
    '    Public Const InvalidRetrievalMode As Short = 11007
    '    Public Const InvalidSecurityArea As Short = 11033
    '    Public Const InvalidTime As Short = 11005
    '    Public Const MaxNtfsAlreadyAdded As Short = 11010
    '    Public Const MemoryError As Short = 11001
    '    Public Const MoreSamplesAvailable As Short = 11012
    '    Public Const NACNotRunning As Short = 11034
    '    Public Const NAMNotPaused As Short = 11036
    '    Public Const NAMNotRunning As Short = 11035
    '    Public Const NodeNameNotDefined As Short = 11014
    '    Public Const NoMessageForError As Short = 11017
    '    Public Const NoNtfsInGroup As Short = 11013
    '    Public Const OK As Short = 11000
    '    Public Const SecurityNotEnabled As Short = 11027
    '    Public Const StringNotLargeEnough As Short = 11009
    '    Public Const TaskNotRegistered As Short = 11015
    '    Public Const UnauthorizedAccess As Short = 11031
    '    Public Const UserAlreadyLoggedIn As Short = 11032
    '    Public Const UserNotFound As Short = 11026
    'End Class
Public Class Form1

    Private _HistHelper As INS_CS_Hist_Helper.UserDefinedFunctions

    Property HistHelper As INS_CS_Hist_Helper.UserDefinedFunctions
        Get
            If _HistHelper Is Nothing Then
                _HistHelper = New INS_CS_Hist_Helper.UserDefinedFunctions
                If _HistHelper.ConnectHistorian(txtSourceServerName.Text, txtSourceUserName.Text, txtSourcePassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
                    Logit("Historian is connected!")
                Else
                    Logit("Historian connection failed")
                End If
            End If
            Return _HistHelper
        End Get
        Set(value As INS_CS_Hist_Helper.UserDefinedFunctions)
            _HistHelper = value
        End Set
    End Property

    Private Property HistorianServerName As String
        Get
            Return txtSourceServerName.Text
        End Get
        Set(value As String)

        End Set
    End Property

    Private Property HistorianUserName As String
        Get
            Return txtSourceUserName.Text
        End Get
        Set(value As String)

        End Set
    End Property

    Private Property HistorianPassword As String
        Get
            Return txtSourcePassword.Text
        End Get
        Set(value As String)

        End Set
    End Property

    Private Property StartTime As String
        Get
            Return dtStartDate.Text
        End Get
        Set(value As String)

        End Set
    End Property

    Private Property EndTime As String
        Get
            Return dtEndDate.Text
        End Get
        Set(value As String)

        End Set
    End Property

    Delegate Sub AddItemDelegate(message As String)

    Private Sub Logit(Msg As String)

        If Me.ListBox1.InvokeRequired Then
            ListBox1.Invoke(New AddItemDelegate(AddressOf Logit), New Object() {Msg})
        Else
            ListBox1.Items.Add(Date.Now().ToString & vbTab & Msg)
            ListBox1.SelectedIndex = ListBox1.Items.Count - 1
        End If

        If chkLogToFile.Checked Then
            WriteToLog(Msg)
        End If

    End Sub

    Protected Sub WriteToLog(ByVal strMessage As String)
        Dim myFileName As String = "MigrationLog-" & DateTime.Now.Year.ToString() & DateTime.Now.Month.ToString() & DateTime.Now.Day.ToString() & "_" & txtSourceServerName.Text & "_To_" & txtDestinationServerName.Text & ".log"
        Dim myStream As New FileStream(myFileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)

        Dim myLog As New StreamWriter(myStream)
        myLog.WriteLine(DateTime.Now.ToString("g") & vbTab & strMessage)
        myLog.Flush()
        myLog.Close()

    End Sub


    Function BrowseTags(server As String, user As String, pass As String, Optional filter As String = "*") As Boolean
        Dim Success As Boolean = False
        Try
            If txtFilter.Text<>"" then
                filter = txtFilter.text
            End If

            Logit("Browsing " & server & " Server Tags")
            lvwTags.Items.Clear()

            _HistHelper = New INS_CS_Hist_Helper.UserDefinedFunctions
            If _HistHelper.ConnectHistorian(server, user, pass) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
                Logit(server & " :: Historian is connected!")
            Else
                Logit(server & " :: Historian connection failed")
            End If


            Dim samples = HistHelper.BrowseHistorianTags(server, user, pass, filter)

            For Each One In samples
                Dim lvitem = New ListViewItem()
                lvitem.SubItems(0).Text = One.TagName
                lvitem.SubItems.Add("")
                if chkSameDestinationName.Checked then
                    lvitem.SubItems(1).Text = one.TagName
                End If
                

                lvwTags.Items.Add(lvitem)
            Next
            Logit("Found " & samples.Count & " tags")

            Success = True
        Catch ex As Exception

        End Try

        Return Success
    End Function


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        txtSourceServerName.Text = System.Net.Dns.GetHostName
        dtStartDate.Value = Convert.ToDateTime(DateAdd(DateInterval.Day, -20, Convert.ToDateTime(Now.ToLongTimeString)))
        cboDebug.SelectedIndex = 0

    End Sub


    Private Sub btnStartExport_Click(sender As Object, e As EventArgs) Handles btnStartExport.Click
        
        Dim sourceHistorian As New proficyHistorianServer(txtSourceServerName.Text, txtSourceUserName.Text, txtSourcePassword.Text)
        Dim destinationHistorian As New proficyHistorianServer(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text)
        Dim tagsMigratedCount As Integer = 0

        'Move Listview into DataTable
        Dim DT As New DataTable
        DT.Columns.Add("Selected")
        DT.Columns.Add("SourceTag")
        DT.Columns.Add("DestinationTag")

        For i = 0 To lvwTags.Items.Count - 1
            If lvwTags.Items(i).Checked Then
                DT.Rows.Add("x", lvwTags.Items(i).SubItems(0).Text, lvwTags.Items(i).SubItems(1).Text)
            Else
                DT.Rows.Add("", lvwTags.Items(i).SubItems(0).Text, lvwTags.Items(i).SubItems(1).Text)
            End If

        Next

        Dim debug As HistorianMigration.DebugType

        If cboDebug.Text = "Debug ON" Then
            debug = HistorianMigration.DebugType.DebugOn
            WriteToLog(">> DEBUG ON")
        Else
            debug = HistorianMigration.DebugType.DebugOff
            WriteToLog(">> debug off")
        End If

        'For i = 0 To Convert.ToInt32(txtThreads.Text) - 1
        WriteToLog("start: " & dtStartDate.Value.ToString & " :: End: " & dtEndDate.Value.ToString)

        If rbHistorianServer.Checked Then
            Dim fullMigration As New HistorianMigration(sourceHistorian, destinationHistorian, DT, dtStartDate.Value, dtEndDate.Value, debug)
            AddHandler fullMigration.MigrationMessaging, AddressOf LogItMessaging
            AddHandler FullMigration.MigrationTagCount, AddressOf LogTagCount
        Else
            Dim ExportMigration As New HistorianCSVExport(sourceHistorian, txtDataFolder.Text, DT, dtStartDate.Value, dtEndDate.Value, debug)
            AddHandler ExportMigration.MigrationMessaging, AddressOf LogItMessaging
        End If


        'If (tagsMigratedCount < lvwTags.Items.Count - 1) Then
        '    Dim tagMigration As New HistorianMigration(sourceHistorian, destinationHistorian, lvwTags.Items(i).SubItems(0).Text, lvwTags.Items(i).SubItems(1).Text, dtStartDate.Value, dtEndDate.Value, debug)
        '    AddHandler tagMigration.MigrationMessaging, AddressOf LogItMessaging

        '    tagsMigratedCount += 1
        'End If

        'Next i



    End Sub

    Private Sub LogItMessaging(message As String)
        Logit(message)
    End Sub
    Private Sub LogTagCount(message As String)
        If message.ToLower() <>"done" then
            ToolStripStatusLabel1.Text = "Completed: " & message & " of " & lvwTags.Items.Count
            ToolStripProgressBar1.Value = (convert.ToInt32(message))/lvwTags.Items.Count*100
        else
            lblResults.Text = "Migration Complete"
            ToolStripProgressBar1.Value = 100
        End if
    End Sub
    Private Sub cmdLoadTagCSV_Click(sender As Object, e As EventArgs)
        LoadTagsFromCSV()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs)
        SaveTagsToCSV()
    End Sub

    Private Function SaveTagsToCSV() As Boolean
        Dim Success As Boolean = False

        Try
            Dim SFD As New SaveFileDialog
            Dim FN As String = ""
            Dim Selected As String, PiPoint As String, TagName As String

            If lvwTags.Items.Count = 0 Then
                MessageBox.Show("No Historian Tags to export", "No Historian Tags", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return True
            End If

            With SFD
                .Filter = "Tag CSV|*.csv|Any|*.*"
                .Title = "Save Tag CSV"
            End With

            If SFD.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim lvItem As ListViewItem

                Using SW As New System.IO.StreamWriter(SFD.FileName, False)
                    SW.WriteLine("Selected,SourceTagname,DestinationTagName")
                    For Each lvItem In lvwTags.Items
                        If lvItem.Checked Then
                            Selected = "x"
                        Else
                            Selected = ""
                        End If
                        PiPoint = lvItem.Text
                        TagName = lvItem.SubItems(1).Text
                        SW.WriteLine(String.Format("{0},{1},{2}", Selected, PiPoint, TagName))

                    Next
                End Using

            End If

            Success = True
        Catch ex As Exception
            Logit("Error::SaveTagsToCSV>> " & ex.Message)
        End Try

        Return Success
    End Function

    Private Function LoadTagsFromCSV() As Boolean
        Dim Success As Boolean = False

        Try
            Dim OFD As New OpenFileDialog
            Dim FileName As String = ""
            Dim RowCount As Integer = 0
            With OFD
                .Filter = "CSV Tag File|*.csv|Any|*.*"
                .Title = "Load Tags From CSV"
                .Multiselect = False
            End With
            Dim Response = OFD.ShowDialog()

            If Response = Windows.Forms.DialogResult.OK Then
                FileName = OFD.FileName
            End If

            Logit("Loading tags from file " & OFD.FileName)
            lvwTags.Items.Clear()


            Dim afile As FileIO.TextFieldParser = New FileIO.TextFieldParser(FileName)
            Dim CurrentRecord As String() ' this array will hold each line of data
            afile.TextFieldType = FileIO.FieldType.Delimited
            afile.Delimiters = New String() {","}
            afile.HasFieldsEnclosedInQuotes = True
            afile.TrimWhiteSpace = True

            ' parse the actual file
            Do While Not afile.EndOfData
                Try
                    CurrentRecord = afile.ReadFields
                    If RowCount > 0 Then
                        Dim lvitem = New ListViewItem(CurrentRecord(1))
                        lvitem.SubItems(0).Text = CurrentRecord(1)
                        lvitem.SubItems.Add(CurrentRecord(2))
                        lvitem.SubItems.Add("")

                        If CurrentRecord(0) <> "" Then
                            lvitem.Checked = True
                        End If
                        lvwTags.Items.Add(lvitem)
                    End If

                    RowCount += 1
                Catch ex As FileIO.MalformedLineException
                    Stop
                End Try
            Loop
            Logit(String.Format("{0} tags were loaded from file.", lvwTags.Items.Count))

            Success = True

            afile.Close()
        Catch ex As Exception
            Logit("Error::LoadTagsFromCSV>> " & ex.Message)
        End Try

        Return Success
    End Function


    Private Sub btnReadState_Click(sender As Object, e As EventArgs)
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False
        Dim archiveName As String = ""
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            If Not pHistorian_Destination.ReadDefaultDatastoreCreateOfflineArchives(archiveName, archivesBit, errorMessage) Then
                'Throw New Exception(errorMessage)
                Logit(errorMessage)
            Else
                If (archivesBit = "1") Then

                    Logit("Migrate Old Data [ON] - Create Offline Archives Bit = 1")

                Else

                    Logit("Migrate Old Data [OFF] - Create Offline Archives Bit = 0")


                End If

            End If

        Else
            Logit("Source Historian connection failed")
        End If


    End Sub

    Private Sub btnCreateOfflineArchives_Click(sender As Object, e As EventArgs)
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False


        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            SetCreateOfflineArchives(pHistorian_Destination)

        Else
            Logit("Source Historian connection failed")
        End If
    End Sub
    Public Function SetCreateOfflineArchives(Hist As INS_CS_Hist_Helper.UserDefinedFunctions) as boolean
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If Not Hist.SetDefaultDatastoreCreateOfflineArchives(True, archivesBit, errorMessage) Then
            'Throw New Exception(errorMessage)
            Logit(errorMessage)
            SetCreateOfflineArchives = false
        Else
            Logit("Destination Historian " & txtDestinationServerName.Text & ": CreateOffline Archives = " & archivesBit)
            SetCreateOfflineArchives = true
        End If

    End Function

    Private Sub btnResetCreationState_Click(sender As Object, e As EventArgs)
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            If Not pHistorian_Destination.SetDefaultDatastoreCreateOfflineArchives(False, archivesBit, errorMessage) Then
                'Throw New Exception(errorMessage)
                Logit(errorMessage)
            Else
                Logit(archivesBit)
            End If


        Else
            Logit("Source Historian connection failed")
        End If
    End Sub

    Private Sub cmdLoadTagCSV_Click_1(sender As Object, e As EventArgs) Handles cmdLoadTagCSV.Click
        LoadTagsFromCSV()
    End Sub

    Private Sub btnGetSourceTags_Click_1(sender As Object, e As EventArgs) Handles btnGetSourceTags.Click
        BrowseTags(txtSourceServerName.Text, txtSourceUserName.Text, txtSourcePassword.Text)
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        SaveTagsToCSV()
    End Sub

    Private Sub btnReadState_Click_1(sender As Object, e As EventArgs) Handles btnReadState.Click
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            ReadOfflineArchivesState(pHistorian_Destination)
        Else
            Logit("Source Historian connection failed")
        End If
    End Sub
    public Function ReadOfflineArchivesState(Hist As INS_CS_Hist_Helper.UserDefinedFunctions) As boolean
        Dim archiveName As String = ""
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If Not Hist.ReadDefaultDatastoreCreateOfflineArchives(archiveName, archivesBit, errorMessage) Then
            'Throw New Exception(errorMessage)
                Logit("ReadOfflineArchivesState Error " & errorMessage)
        Else
            If (archivesBit = "1") Then
                Logit("Migrate Old Data [ON] - Create Offline Archives Bit = 1")
                ReadOfflineArchivesState = True
            Else
                Logit("Migrate Old Data [OFF] - Create Offline Archives Bit = 0")
                ReadOfflineArchivesState = False
            End If
        End If
    End Function

    Private Sub btnGetDestinationTags_Click(sender As Object, e As EventArgs) Handles btnGetDestinationTags.Click
        BrowseTags(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text)
    End Sub

    Private Sub cmdBrowseFolder_Click(sender As Object, e As EventArgs) Handles cmdBrowseFolder.Click
        If (DataFolderBrowser.ShowDialog()) = DialogResult.OK Then
            txtDataFolder.Text = DataFolderBrowser.SelectedPath
        End If
    End Sub

    Private Sub rbCSVFile_CheckedChanged(sender As Object, e As EventArgs) Handles rbCSVFile.CheckedChanged
        If rbCSVFile.Checked Then
            txtDataFolder.Enabled = True
            txtDestinationServerName.Enabled = False
            txtDestinationUsername.Enabled = False
            txtDestinationPassword.Enabled = False
        End If
    End Sub

    Private Sub rbHistorianServer_CheckedChanged(sender As Object, e As EventArgs) Handles rbHistorianServer.CheckedChanged
        If rbHistorianServer.Checked Then
            txtDataFolder.Enabled = False
            txtDestinationServerName.Enabled = True
            txtDestinationUsername.Enabled = True
            txtDestinationPassword.Enabled = True
        End If
    End Sub

    Private Sub btnCSVQueryTags_Click(sender As Object, e As EventArgs) Handles btnCSVQueryTags.Click
        BrowseTags(txtSourceServerName.Text, txtSourceUserName.Text, txtSourcePassword.Text, txtCSVFilterQuery.Text)
    End Sub

    Private Sub btnCSVSaveCSV_Click(sender As Object, e As EventArgs) Handles btnCSVSaveCSV.Click
        SaveTagsToCSV()
    End Sub

    Private Sub btnCSVLoadCSV_Click(sender As Object, e As EventArgs) Handles btnCSVLoadCSV.Click
        LoadTagsFromCSV()
    End Sub

    Private Sub CheckedListBox1_DragDrop(sender As Object, e As DragEventArgs) Handles CheckedListBox1.DragDrop
        CheckedListBox1.Items.Insert(CheckedListBox1.IndexFromPoint(CheckedListBox1.PointToClient(New Point(e.X, e.Y))), e.Data.GetData(DataFormats.Text))
        CheckedListBox1.Items.RemoveAt(CheckedListBox1.SelectedIndex)
    End Sub

    Private Sub CheckedListBox1_DragOver(sender As Object, e As DragEventArgs) Handles CheckedListBox1.DragOver
        e.Effect = DragDropEffects.Move
    End Sub

    Private Sub CheckedListBox1_MouseDown(sender As Object, e As MouseEventArgs) Handles CheckedListBox1.MouseDown
        CheckedListBox1.DoDragDrop(CheckedListBox1.Text, DragDropEffects.All)
    End Sub

    Private Sub miSelectAll_Click(sender As Object, e As EventArgs) Handles miSelectAll.Click
        lvwTags.BeginUpdate()
        For each li As ListViewItem In lvwTags.items
                li.Checked = true
        next
        lvwTags.EndUpdate()
    End Sub

    Private Sub miClearAll_Click(sender As Object, e As EventArgs) Handles miClearAll.Click
        lvwTags.BeginUpdate()
        For each li As ListViewItem In lvwTags.items
            li.Checked = false
        next
        lvwTags.EndUpdate()
    End Sub

    Private Sub btnCreateOfflineArchives_Click_1(sender As Object, e As EventArgs) Handles btnCreateOfflineArchives.Click
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            If Not pHistorian_Destination.SetDefaultDatastoreCreateOfflineArchives(True, archivesBit, errorMessage) Then
                'Throw New Exception(errorMessage)
                Logit(errorMessage)
            Else
                Logit(archivesBit)
            End If


        Else
            Logit("Source Historian connection failed")
        End If
    End Sub

    Private Sub btnResetCreationState_Click_1(sender As Object, e As EventArgs) Handles btnResetCreationState.Click
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions
        Dim blnSourceHistorianConnected As Boolean = False
        Dim archivesBit As String = ""
        Dim errorMessage As String = ""

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
            blnSourceHistorianConnected = True

            If Not pHistorian_Destination.SetDefaultDatastoreCreateOfflineArchives(False, archivesBit, errorMessage) Then
                'Throw New Exception(errorMessage)
                Logit(errorMessage)
            Else
                Logit(archivesBit)
            End If


        Else
            Logit("Source Historian connection failed")
        End If
    End Sub

    Private Sub btnTestSourceHistorian_Click(sender As Object, e As EventArgs) Handles btnTestSourceHistorian.Click
        Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions

        If pHistorian_Source.ConnectHistorian(txtSourceServerName.Text, txtSourceUsername.Text, txtSourcePassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Source Historian is connected!")
        else
            Logit("Source Historian is connected!")
        end if
    End Sub

    Private Sub btnTestDestinationHistorian_Click(sender As Object, e As EventArgs) Handles btnTestDestinationHistorian.Click
        Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions

        If pHistorian_Destination.ConnectHistorian(txtDestinationServerName.Text, txtDestinationUsername.Text, txtDestinationPassword.Text) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            Logit("Destination Historian is connected!")
        else
            Logit("Destination Historian is connected!")
        end if
    End Sub

    Private Sub GetHTCTags()
    
    Dim icount As Int32
    Dim i As Integer
    Dim sbNtf As String = String.Empty
    Dim sNodeName As String
    Dim m_ighandle As Int32 = 0 ' Group handle for HDA reads
    dim m_nerror As Int32 = 0

    Try
        sNodeName = "FCESCADA"

        m_nerror = Proficy.iFixToolkit.Adapter2.Hda.EnumNtfs(m_ighandle, sNodeName, icount)
        SetError("HdaEnumNtfs", m_nerror)

        lvwtags.Items.Clear()
            If m_nerror = 11000 Then
                For i = 0 To icount - 1
                    m_nerror = Proficy.iFixToolkit.Adapter2.Hda.EnumGetNtf(m_ighandle, i, sbNtf)
                    SetError("HdaEnumNtfs", m_nerror)
                    If m_nerror = 11000 Then
                        lvwtags.Items.Add(sbNtf)
                    End If
                Next
            End If
        Catch ex As Exception
        MsgBox(ex.Message)
    End Try


    End Sub

        Private Sub SetError(ByVal func As String, ByVal ErrNo As Int32)
        'Txt_Function.Text = func
            DIM Txt_HdaError as new TextBox

        'Select Case ErrNo
        '    Case ToolkitError.OK
        '        Txt_HdaError.Text = "ToolkitOK"
        '    Case ToolkitError.MemoryError
        '        Txt_HdaError.Text = "ToolkitMemoryError"
        '    Case ToolkitError.BadHdaGroup
        '        Txt_HdaError.Text = "ToolkitBadHdaGroup"
        '    Case ToolkitError.BadHdaNtf
        '        Txt_HdaError.Text = "ToolkitBadHdaNtf"
        '    Case ToolkitError.InvalidDate
        '        Txt_HdaError.Text = "ToolkitInvalidDate"
        '    Case ToolkitError.InvalidTime
        '        Txt_HdaError.Text = "ToolkitInvalidTime"
        '    Case ToolkitError.IndexOutOfRange
        '        Txt_HdaError.Text = "ToolkitIndexOutOfRange"
        '    Case ToolkitError.InvalidRetrievalMode
        '        Txt_HdaError.Text = "ToolkitInvalidRetrievalMode"
        '    Case ToolkitError.InvalidNtfFormat
        '        Txt_HdaError.Text = "ToolkitInvalidNtfFormat"
        '    Case ToolkitError.StringNotLargeEnough
        '        Txt_HdaError.Text = "ToolkitStringNotLargeEnough"
        '    Case ToolkitError.MaxNtfsAlreadyAdded
        '        Txt_HdaError.Text = "ToolkitMaxNtfsAlreadyAdded"
        '    Case ToolkitError.InvalidPointer
        '        Txt_HdaError.Text = "ToolkitInvalidPointer"
        '    Case ToolkitError.MoreSamplesAvailable
        '        Txt_HdaError.Text = "ToolkitMoreSamplesAvailable"
        '    Case ToolkitError.NoNtfsInGroup
        '        Txt_HdaError.Text = "ToolkitNoNtfsInGroup"
        '    Case ToolkitError.NodeNameNotDefined
        '        Txt_HdaError.Text = "ToolkitNodeNameNotDefined"
        '    Case ToolkitError.TaskNotRegistered
        '        Txt_HdaError.Text = "ToolkitTaskNotRegistered"
        '    Case ToolkitError.BadRegistrationOrder
        '        Txt_HdaError.Text = "ToolkitBadRegistrationOrder"
        '    Case ToolkitError.NoMessageForError
        '        Txt_HdaError.Text = "ToolkitNoMessageForError"
        '    Case ToolkitError.FixNotRunning
        '        Txt_HdaError.Text = "ToolkitFixNotRunning"
        '    Case ToolkitError.InvalidProgramName
        '        Txt_HdaError.Text = "ToolkitInvalidProgramName"
        '    Case ToolkitError.InvalidPath
        '        Txt_HdaError.Text = "ToolkitInvalidPath"
        '    Case ToolkitError.InvalidNodeName
        '        Txt_HdaError.Text = "ToolkitInvalidNodeName"
        '    Case ToolkitError.DataNotReadyYet
        '        Txt_HdaError.Text = "ToolkitDataNotReadyYet"
        '    Case Else
        '        Txt_HdaError.Text = "Unrecognized error"
        'End Select
    End Sub

    Private Sub btnGetTags_Click(sender As Object, e As EventArgs) Handles btnGetTags.Click
        GetHTCTags
    End Sub

    Private Sub chkSameDestinationName_CheckedChanged(sender As Object, e As EventArgs) Handles chkSameDestinationName.CheckedChanged
        If chkSameDestinationName.Checked then
            For each tag As listviewitem in lvwTags.Items
                tag.SubItems(1).Text = tag.SubItems(0).text
            Next
        else
            For each tag As listviewitem in lvwTags.Items
                tag.SubItems(1).Text = ""
            Next
        End If
    End Sub
End Class
