﻿Public Class LBItem
    Implements IEquatable(Of LBItem)

    Private _Id As String
    Private _Description As String

    Public Property Id As String
        Get
            Return _Id
        End Get
        Set(value As String)
            _Id = value
        End Set
    End Property

    Public Property Description As String
        Get
            Return _Description
        End Get
        Set(value As String)
            _Description = value
        End Set
    End Property

    Sub New()

    End Sub

    Sub New(Id As String, Description As String)
        _Id = Id
        _Description = Description
    End Sub

    Public Overrides Function ToString() As String
        Return _Description
    End Function

    Public Function Equals1(other As LBItem) As Boolean Implements System.IEquatable(Of LBItem).Equals
        If other Is Nothing Then Return False
        If Me Is other Then Return True
        Return Id.Equals(other.Id) AndAlso Description.Equals(other.Description)
    End Function
End Class