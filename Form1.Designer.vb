﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.lblResults = New System.Windows.Forms.Label()
        Me.btnStartExport = New System.Windows.Forms.Button()
        Me.lblStartDate = New System.Windows.Forms.Label()
        Me.dtStartDate = New System.Windows.Forms.DateTimePicker()
        Me.lblEndDate = New System.Windows.Forms.Label()
        Me.dtEndDate = New System.Windows.Forms.DateTimePicker()
        Me.lvwTags = New System.Windows.Forms.ListView()
        Me.SourceTagName = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.DestinationTagName = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.lvMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miSelectAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.miClearAll = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnTestDestinationHistorian = New System.Windows.Forms.Button()
        Me.cmdBrowseFolder = New System.Windows.Forms.Button()
        Me.txtDataFolder = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.rbCSVFile = New System.Windows.Forms.RadioButton()
        Me.rbHistorianServer = New System.Windows.Forms.RadioButton()
        Me.txtDestinationPassword = New System.Windows.Forms.TextBox()
        Me.txtDestinationUsername = New System.Windows.Forms.TextBox()
        Me.txtDestinationServerName = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnTestSourceHistorian = New System.Windows.Forms.Button()
        Me.txtSourcePassword = New System.Windows.Forms.TextBox()
        Me.txtSourceUserName = New System.Windows.Forms.TextBox()
        Me.txtSourceServerName = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnGetDestinationTags = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnGetSourceTags = New System.Windows.Forms.Button()
        Me.cmdLoadTagCSV = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboDebug = New System.Windows.Forms.ComboBox()
        Me.chkLogToFile = New System.Windows.Forms.CheckBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnReadState = New System.Windows.Forms.Button()
        Me.btnCreateOfflineArchives = New System.Windows.Forms.Button()
        Me.btnResetCreationState = New System.Windows.Forms.Button()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtThreads = New System.Windows.Forms.TextBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnCSVSaveCSV = New System.Windows.Forms.Button()
        Me.btnCSVLoadCSV = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtCSVFilterQuery = New System.Windows.Forms.TextBox()
        Me.btnCSVQueryTags = New System.Windows.Forms.Button()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.btnGetTags = New System.Windows.Forms.Button()
        Me.DataFolderBrowser = New System.Windows.Forms.FolderBrowserDialog()
        Me.chkBulkWrite = New System.Windows.Forms.CheckBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtFilter = New System.Windows.Forms.TextBox()
        Me.chkSameDestinationName = New System.Windows.Forms.CheckBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.lvMenu.SuspendLayout
        Me.TabControl1.SuspendLayout
        Me.TabPage1.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.TabPage2.SuspendLayout
        Me.TabPage3.SuspendLayout
        Me.TabPage4.SuspendLayout
        Me.GroupBox3.SuspendLayout
        Me.TabPage5.SuspendLayout
        Me.TabPage6.SuspendLayout
        Me.GroupBox6.SuspendLayout
        Me.GroupBox5.SuspendLayout
        Me.GroupBox4.SuspendLayout
        Me.TabPage7.SuspendLayout
        Me.GroupBox7.SuspendLayout
        Me.StatusStrip1.SuspendLayout
        Me.SuspendLayout
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.ListBox1.FormattingEnabled = true
        Me.ListBox1.Location = New System.Drawing.Point(9, 473)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(885, 173)
        Me.ListBox1.TabIndex = 13
        '
        'lblResults
        '
        Me.lblResults.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblResults.AutoSize = true
        Me.lblResults.Location = New System.Drawing.Point(11, 670)
        Me.lblResults.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblResults.Name = "lblResults"
        Me.lblResults.Size = New System.Drawing.Size(42, 13)
        Me.lblResults.TabIndex = 26
        Me.lblResults.Text = "Results"
        '
        'btnStartExport
        '
        Me.btnStartExport.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnStartExport.Location = New System.Drawing.Point(819, 664)
        Me.btnStartExport.Name = "btnStartExport"
        Me.btnStartExport.Size = New System.Drawing.Size(75, 23)
        Me.btnStartExport.TabIndex = 28
        Me.btnStartExport.Text = "Start Export"
        Me.btnStartExport.UseVisualStyleBackColor = true
        '
        'lblStartDate
        '
        Me.lblStartDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblStartDate.AutoSize = true
        Me.lblStartDate.Location = New System.Drawing.Point(429, 669)
        Me.lblStartDate.Name = "lblStartDate"
        Me.lblStartDate.Size = New System.Drawing.Size(32, 13)
        Me.lblStartDate.TabIndex = 31
        Me.lblStartDate.Text = "Start:"
        '
        'dtStartDate
        '
        Me.dtStartDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dtStartDate.CustomFormat = "yyyy-MM-dd HH:mm:ss"
        Me.dtStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtStartDate.Location = New System.Drawing.Point(466, 666)
        Me.dtStartDate.Name = "dtStartDate"
        Me.dtStartDate.Size = New System.Drawing.Size(141, 20)
        Me.dtStartDate.TabIndex = 34
        '
        'lblEndDate
        '
        Me.lblEndDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.lblEndDate.AutoSize = true
        Me.lblEndDate.Location = New System.Drawing.Point(620, 669)
        Me.lblEndDate.Name = "lblEndDate"
        Me.lblEndDate.Size = New System.Drawing.Size(29, 13)
        Me.lblEndDate.TabIndex = 32
        Me.lblEndDate.Text = "End:"
        '
        'dtEndDate
        '
        Me.dtEndDate.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.dtEndDate.CustomFormat = "yyyy-MM-dd HH:mm:ss"
        Me.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtEndDate.Location = New System.Drawing.Point(653, 666)
        Me.dtEndDate.Name = "dtEndDate"
        Me.dtEndDate.Size = New System.Drawing.Size(160, 20)
        Me.dtEndDate.TabIndex = 33
        '
        'lvwTags
        '
        Me.lvwTags.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lvwTags.CheckBoxes = true
        Me.lvwTags.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SourceTagName, Me.DestinationTagName})
        Me.lvwTags.ContextMenuStrip = Me.lvMenu
        Me.lvwTags.Location = New System.Drawing.Point(9, 177)
        Me.lvwTags.MultiSelect = false
        Me.lvwTags.Name = "lvwTags"
        Me.lvwTags.Size = New System.Drawing.Size(885, 288)
        Me.lvwTags.TabIndex = 37
        Me.lvwTags.UseCompatibleStateImageBehavior = false
        Me.lvwTags.View = System.Windows.Forms.View.Details
        '
        'SourceTagName
        '
        Me.SourceTagName.Text = "Source Tag Name"
        Me.SourceTagName.Width = 493
        '
        'DestinationTagName
        '
        Me.DestinationTagName.Text = "Destination Tag Name"
        Me.DestinationTagName.Width = 501
        '
        'lvMenu
        '
        Me.lvMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miSelectAll, Me.miClearAll})
        Me.lvMenu.Name = "lvMenu"
        Me.lvMenu.Size = New System.Drawing.Size(123, 48)
        '
        'miSelectAll
        '
        Me.miSelectAll.Name = "miSelectAll"
        Me.miSelectAll.Size = New System.Drawing.Size(122, 22)
        Me.miSelectAll.Text = "Select All"
        '
        'miClearAll
        '
        Me.miClearAll.Name = "miClearAll"
        Me.miClearAll.Size = New System.Drawing.Size(122, 22)
        Me.miClearAll.Text = "Clear All"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Location = New System.Drawing.Point(9, -1)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(885, 172)
        Me.TabControl1.TabIndex = 53
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(877, 146)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Historian Server Setup"
        Me.TabPage1.UseVisualStyleBackColor = true
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnTestDestinationHistorian)
        Me.GroupBox2.Controls.Add(Me.cmdBrowseFolder)
        Me.GroupBox2.Controls.Add(Me.txtDataFolder)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.rbCSVFile)
        Me.GroupBox2.Controls.Add(Me.rbHistorianServer)
        Me.GroupBox2.Controls.Add(Me.txtDestinationPassword)
        Me.GroupBox2.Controls.Add(Me.txtDestinationUsername)
        Me.GroupBox2.Controls.Add(Me.txtDestinationServerName)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(238, 15)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(618, 128)
        Me.GroupBox2.TabIndex = 31
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Destination"
        '
        'btnTestDestinationHistorian
        '
        Me.btnTestDestinationHistorian.Location = New System.Drawing.Point(246, 102)
        Me.btnTestDestinationHistorian.Name = "btnTestDestinationHistorian"
        Me.btnTestDestinationHistorian.Size = New System.Drawing.Size(45, 23)
        Me.btnTestDestinationHistorian.TabIndex = 17
        Me.btnTestDestinationHistorian.Text = "test"
        Me.btnTestDestinationHistorian.UseVisualStyleBackColor = true
        '
        'cmdBrowseFolder
        '
        Me.cmdBrowseFolder.Location = New System.Drawing.Point(547, 35)
        Me.cmdBrowseFolder.Name = "cmdBrowseFolder"
        Me.cmdBrowseFolder.Size = New System.Drawing.Size(63, 22)
        Me.cmdBrowseFolder.TabIndex = 20
        Me.cmdBrowseFolder.Text = "Browse"
        Me.cmdBrowseFolder.UseVisualStyleBackColor = true
        '
        'txtDataFolder
        '
        Me.txtDataFolder.Location = New System.Drawing.Point(385, 36)
        Me.txtDataFolder.Name = "txtDataFolder"
        Me.txtDataFolder.Size = New System.Drawing.Size(155, 20)
        Me.txtDataFolder.TabIndex = 19
        '
        'Label14
        '
        Me.Label14.AutoSize = true
        Me.Label14.Location = New System.Drawing.Point(314, 39)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(65, 13)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Data Folder:"
        '
        'rbCSVFile
        '
        Me.rbCSVFile.AutoSize = true
        Me.rbCSVFile.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbCSVFile.Location = New System.Drawing.Point(327, 8)
        Me.rbCSVFile.Name = "rbCSVFile"
        Me.rbCSVFile.Size = New System.Drawing.Size(89, 24)
        Me.rbCSVFile.TabIndex = 17
        Me.rbCSVFile.TabStop = true
        Me.rbCSVFile.Text = "CSV File"
        Me.rbCSVFile.UseVisualStyleBackColor = true
        '
        'rbHistorianServer
        '
        Me.rbHistorianServer.AutoSize = true
        Me.rbHistorianServer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.rbHistorianServer.Location = New System.Drawing.Point(70, 8)
        Me.rbHistorianServer.Name = "rbHistorianServer"
        Me.rbHistorianServer.Size = New System.Drawing.Size(140, 24)
        Me.rbHistorianServer.TabIndex = 16
        Me.rbHistorianServer.TabStop = true
        Me.rbHistorianServer.Text = "Historian Server"
        Me.rbHistorianServer.UseVisualStyleBackColor = true
        '
        'txtDestinationPassword
        '
        Me.txtDestinationPassword.Location = New System.Drawing.Point(140, 90)
        Me.txtDestinationPassword.Name = "txtDestinationPassword"
        Me.txtDestinationPassword.Size = New System.Drawing.Size(100, 20)
        Me.txtDestinationPassword.TabIndex = 15
        '
        'txtDestinationUsername
        '
        Me.txtDestinationUsername.Location = New System.Drawing.Point(140, 63)
        Me.txtDestinationUsername.Name = "txtDestinationUsername"
        Me.txtDestinationUsername.Size = New System.Drawing.Size(100, 20)
        Me.txtDestinationUsername.TabIndex = 14
        '
        'txtDestinationServerName
        '
        Me.txtDestinationServerName.Location = New System.Drawing.Point(140, 36)
        Me.txtDestinationServerName.Name = "txtDestinationServerName"
        Me.txtDestinationServerName.Size = New System.Drawing.Size(100, 20)
        Me.txtDestinationServerName.TabIndex = 13
        Me.txtDestinationServerName.Text = "VS2012-64BIT"
        '
        'Label6
        '
        Me.Label6.AutoSize = true
        Me.Label6.Location = New System.Drawing.Point(67, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Password:"
        '
        'Label7
        '
        Me.Label7.AutoSize = true
        Me.Label7.Location = New System.Drawing.Point(67, 66)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Username:"
        '
        'Label8
        '
        Me.Label8.AutoSize = true
        Me.Label8.Location = New System.Drawing.Point(67, 39)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 13)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "ServerName:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnTestSourceHistorian)
        Me.GroupBox1.Controls.Add(Me.txtSourcePassword)
        Me.GroupBox1.Controls.Add(Me.txtSourceUserName)
        Me.GroupBox1.Controls.Add(Me.txtSourceServerName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 15)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 128)
        Me.GroupBox1.TabIndex = 30
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Source Historian"
        '
        'btnTestSourceHistorian
        '
        Me.btnTestSourceHistorian.Location = New System.Drawing.Point(149, 102)
        Me.btnTestSourceHistorian.Name = "btnTestSourceHistorian"
        Me.btnTestSourceHistorian.Size = New System.Drawing.Size(45, 23)
        Me.btnTestSourceHistorian.TabIndex = 16
        Me.btnTestSourceHistorian.Text = "test"
        Me.btnTestSourceHistorian.UseVisualStyleBackColor = true
        '
        'txtSourcePassword
        '
        Me.txtSourcePassword.Location = New System.Drawing.Point(87, 76)
        Me.txtSourcePassword.Name = "txtSourcePassword"
        Me.txtSourcePassword.Size = New System.Drawing.Size(100, 20)
        Me.txtSourcePassword.TabIndex = 15
        '
        'txtSourceUserName
        '
        Me.txtSourceUserName.Location = New System.Drawing.Point(87, 49)
        Me.txtSourceUserName.Name = "txtSourceUserName"
        Me.txtSourceUserName.Size = New System.Drawing.Size(100, 20)
        Me.txtSourceUserName.TabIndex = 14
        '
        'txtSourceServerName
        '
        Me.txtSourceServerName.Location = New System.Drawing.Point(87, 22)
        Me.txtSourceServerName.Name = "txtSourceServerName"
        Me.txtSourceServerName.Size = New System.Drawing.Size(100, 20)
        Me.txtSourceServerName.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.AutoSize = true
        Me.Label3.Location = New System.Drawing.Point(14, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Password:"
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(14, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Username:"
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(14, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "ServerName:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.chkSameDestinationName)
        Me.TabPage2.Controls.Add(Me.GroupBox7)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.btnGetDestinationTags)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.btnGetSourceTags)
        Me.TabPage2.Controls.Add(Me.cmdLoadTagCSV)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(877, 146)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Tag Setup"
        Me.TabPage2.UseVisualStyleBackColor = true
        '
        'Label15
        '
        Me.Label15.AutoSize = true
        Me.Label15.Font = New System.Drawing.Font("Arial Narrow", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Maroon
        Me.Label15.Location = New System.Drawing.Point(53, 60)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(155, 16)
        Me.Label15.TabIndex = 60
        Me.Label15.Text = "(Not Required for CSV Export)"
        '
        'btnGetDestinationTags
        '
        Me.btnGetDestinationTags.Location = New System.Drawing.Point(209, 41)
        Me.btnGetDestinationTags.Name = "btnGetDestinationTags"
        Me.btnGetDestinationTags.Size = New System.Drawing.Size(122, 23)
        Me.btnGetDestinationTags.TabIndex = 59
        Me.btnGetDestinationTags.Text = "Get Destination Tags"
        Me.btnGetDestinationTags.UseVisualStyleBackColor = true
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(23, 47)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(170, 13)
        Me.Label13.TabIndex = 58
        Me.Label13.Text = "Step 2:  Get Tags from Destination"
        '
        'Label11
        '
        Me.Label11.AutoSize = true
        Me.Label11.Location = New System.Drawing.Point(551, 51)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(149, 13)
        Me.Label11.TabIndex = 57
        Me.Label11.Text = "Step 5:  Load in Modified CSV"
        '
        'Label10
        '
        Me.Label10.AutoSize = true
        Me.Label10.Location = New System.Drawing.Point(551, 33)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(224, 13)
        Me.Label10.TabIndex = 56
        Me.Label10.Text = "Step 4: Open CSV File to add Destination Tag"
        '
        'Label9
        '
        Me.Label9.AutoSize = true
        Me.Label9.Location = New System.Drawing.Point(23, 103)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(128, 13)
        Me.Label9.TabIndex = 55
        Me.Label9.Text = "Step 3: Save tags to CSV"
        '
        'Label5
        '
        Me.Label5.AutoSize = true
        Me.Label5.Location = New System.Drawing.Point(23, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(151, 13)
        Me.Label5.TabIndex = 54
        Me.Label5.Text = "Step 1:  Get Tags from Source"
        '
        'btnGetSourceTags
        '
        Me.btnGetSourceTags.Location = New System.Drawing.Point(209, 9)
        Me.btnGetSourceTags.Name = "btnGetSourceTags"
        Me.btnGetSourceTags.Size = New System.Drawing.Size(122, 23)
        Me.btnGetSourceTags.TabIndex = 53
        Me.btnGetSourceTags.Text = "Get Source Tags"
        Me.btnGetSourceTags.UseVisualStyleBackColor = true
        '
        'cmdLoadTagCSV
        '
        Me.cmdLoadTagCSV.Location = New System.Drawing.Point(595, 72)
        Me.cmdLoadTagCSV.Name = "cmdLoadTagCSV"
        Me.cmdLoadTagCSV.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoadTagCSV.TabIndex = 51
        Me.cmdLoadTagCSV.Text = "Load CSV"
        Me.cmdLoadTagCSV.UseVisualStyleBackColor = true
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(209, 94)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 52
        Me.Button2.Text = "Save CSV"
        Me.Button2.UseVisualStyleBackColor = true
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.cboDebug)
        Me.TabPage3.Controls.Add(Me.chkLogToFile)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(877, 146)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Debug Log Setup"
        Me.TabPage3.UseVisualStyleBackColor = true
        '
        'Label12
        '
        Me.Label12.AutoSize = true
        Me.Label12.Location = New System.Drawing.Point(8, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(92, 13)
        Me.Label12.TabIndex = 55
        Me.Label12.Text = "Debug Log Level:"
        '
        'cboDebug
        '
        Me.cboDebug.FormattingEnabled = true
        Me.cboDebug.Items.AddRange(New Object() {"Debug OFF", "Debug ON"})
        Me.cboDebug.Location = New System.Drawing.Point(106, 13)
        Me.cboDebug.Name = "cboDebug"
        Me.cboDebug.Size = New System.Drawing.Size(202, 21)
        Me.cboDebug.TabIndex = 54
        '
        'chkLogToFile
        '
        Me.chkLogToFile.AutoSize = true
        Me.chkLogToFile.Checked = true
        Me.chkLogToFile.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLogToFile.Location = New System.Drawing.Point(106, 57)
        Me.chkLogToFile.Name = "chkLogToFile"
        Me.chkLogToFile.Size = New System.Drawing.Size(200, 17)
        Me.chkLogToFile.TabIndex = 53
        Me.chkLogToFile.Text = "Log to File (Takes Longer to Migrate)"
        Me.chkLogToFile.UseVisualStyleBackColor = true
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox3)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(877, 146)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Archive Creation Bit"
        Me.TabPage4.UseVisualStyleBackColor = true
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnReadState)
        Me.GroupBox3.Controls.Add(Me.btnCreateOfflineArchives)
        Me.GroupBox3.Controls.Add(Me.btnResetCreationState)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(184, 100)
        Me.GroupBox3.TabIndex = 47
        Me.GroupBox3.TabStop = false
        Me.GroupBox3.Text = "Archive Creation Bit"
        '
        'btnReadState
        '
        Me.btnReadState.Location = New System.Drawing.Point(6, 19)
        Me.btnReadState.Name = "btnReadState"
        Me.btnReadState.Size = New System.Drawing.Size(175, 23)
        Me.btnReadState.TabIndex = 40
        Me.btnReadState.Text = "Read Creation State"
        Me.btnReadState.UseVisualStyleBackColor = true
        '
        'btnCreateOfflineArchives
        '
        Me.btnCreateOfflineArchives.Location = New System.Drawing.Point(6, 48)
        Me.btnCreateOfflineArchives.Name = "btnCreateOfflineArchives"
        Me.btnCreateOfflineArchives.Size = New System.Drawing.Size(175, 23)
        Me.btnCreateOfflineArchives.TabIndex = 41
        Me.btnCreateOfflineArchives.Text = "SET Creation State"
        Me.btnCreateOfflineArchives.UseVisualStyleBackColor = true
        '
        'btnResetCreationState
        '
        Me.btnResetCreationState.Location = New System.Drawing.Point(6, 77)
        Me.btnResetCreationState.Name = "btnResetCreationState"
        Me.btnResetCreationState.Size = New System.Drawing.Size(175, 23)
        Me.btnResetCreationState.TabIndex = 42
        Me.btnResetCreationState.Text = "Reset Creation State"
        Me.btnResetCreationState.UseVisualStyleBackColor = true
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label4)
        Me.TabPage5.Controls.Add(Me.txtThreads)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(877, 146)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Speed Setup"
        Me.TabPage5.UseVisualStyleBackColor = true
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = true
        Me.Label4.Location = New System.Drawing.Point(16, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 47
        Me.Label4.Text = "Processor Threads:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtThreads
        '
        Me.txtThreads.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.txtThreads.Location = New System.Drawing.Point(121, 17)
        Me.txtThreads.Name = "txtThreads"
        Me.txtThreads.Size = New System.Drawing.Size(49, 20)
        Me.txtThreads.TabIndex = 46
        Me.txtThreads.Text = "1"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.GroupBox6)
        Me.TabPage6.Controls.Add(Me.GroupBox5)
        Me.TabPage6.Controls.Add(Me.GroupBox4)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(877, 146)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "CSV Export Configuration"
        Me.TabPage6.UseVisualStyleBackColor = true
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.CheckedListBox1)
        Me.GroupBox6.Location = New System.Drawing.Point(419, 6)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(381, 134)
        Me.GroupBox6.TabIndex = 71
        Me.GroupBox6.TabStop = false
        Me.GroupBox6.Text = "CSV Output Settings"
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.AllowDrop = true
        Me.CheckedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CheckedListBox1.FormattingEnabled = true
        Me.CheckedListBox1.Items.AddRange(New Object() {"Tagname", "Timestamp", "Value", "Quality", "Historian Server Name"})
        Me.CheckedListBox1.Location = New System.Drawing.Point(16, 22)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(141, 90)
        Me.CheckedListBox1.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnCSVSaveCSV)
        Me.GroupBox5.Controls.Add(Me.btnCSVLoadCSV)
        Me.GroupBox5.Location = New System.Drawing.Point(10, 75)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(378, 65)
        Me.GroupBox5.TabIndex = 70
        Me.GroupBox5.TabStop = false
        Me.GroupBox5.Text = "Tag Configuration CSV"
        '
        'btnCSVSaveCSV
        '
        Me.btnCSVSaveCSV.Location = New System.Drawing.Point(18, 19)
        Me.btnCSVSaveCSV.Name = "btnCSVSaveCSV"
        Me.btnCSVSaveCSV.Size = New System.Drawing.Size(122, 23)
        Me.btnCSVSaveCSV.TabIndex = 62
        Me.btnCSVSaveCSV.Text = "Save CSV"
        Me.btnCSVSaveCSV.UseVisualStyleBackColor = true
        '
        'btnCSVLoadCSV
        '
        Me.btnCSVLoadCSV.Location = New System.Drawing.Point(155, 19)
        Me.btnCSVLoadCSV.Name = "btnCSVLoadCSV"
        Me.btnCSVLoadCSV.Size = New System.Drawing.Size(122, 23)
        Me.btnCSVLoadCSV.TabIndex = 61
        Me.btnCSVLoadCSV.Text = "Load CSV"
        Me.btnCSVLoadCSV.UseVisualStyleBackColor = true
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtCSVFilterQuery)
        Me.GroupBox4.Controls.Add(Me.btnCSVQueryTags)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Location = New System.Drawing.Point(10, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(378, 63)
        Me.GroupBox4.TabIndex = 69
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "Historian Tags"
        '
        'txtCSVFilterQuery
        '
        Me.txtCSVFilterQuery.Location = New System.Drawing.Point(73, 19)
        Me.txtCSVFilterQuery.Name = "txtCSVFilterQuery"
        Me.txtCSVFilterQuery.Size = New System.Drawing.Size(154, 20)
        Me.txtCSVFilterQuery.TabIndex = 68
        '
        'btnCSVQueryTags
        '
        Me.btnCSVQueryTags.Location = New System.Drawing.Point(243, 17)
        Me.btnCSVQueryTags.Name = "btnCSVQueryTags"
        Me.btnCSVQueryTags.Size = New System.Drawing.Size(122, 23)
        Me.btnCSVQueryTags.TabIndex = 63
        Me.btnCSVQueryTags.Text = "Query Historian Tags"
        Me.btnCSVQueryTags.UseVisualStyleBackColor = true
        '
        'Label21
        '
        Me.Label21.AutoSize = true
        Me.Label21.Location = New System.Drawing.Point(17, 25)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(54, 13)
        Me.Label21.TabIndex = 64
        Me.Label21.Text = "Tag Filter:"
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.btnGetTags)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(877, 146)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Classic Historian HTC"
        Me.TabPage7.UseVisualStyleBackColor = true
        '
        'btnGetTags
        '
        Me.btnGetTags.Location = New System.Drawing.Point(23, 18)
        Me.btnGetTags.Name = "btnGetTags"
        Me.btnGetTags.Size = New System.Drawing.Size(86, 24)
        Me.btnGetTags.TabIndex = 0
        Me.btnGetTags.Text = "Get Tags"
        Me.btnGetTags.UseVisualStyleBackColor = true
        '
        'chkBulkWrite
        '
        Me.chkBulkWrite.AutoSize = true
        Me.chkBulkWrite.Location = New System.Drawing.Point(134, 666)
        Me.chkBulkWrite.Name = "chkBulkWrite"
        Me.chkBulkWrite.Size = New System.Drawing.Size(292, 17)
        Me.chkBulkWrite.TabIndex = 54
        Me.chkBulkWrite.Text = "Use Bulk Write (doesn't work on all versions of Historian)"
        Me.chkBulkWrite.UseVisualStyleBackColor = true
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtFilter)
        Me.GroupBox7.Location = New System.Drawing.Point(345, 14)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(200, 62)
        Me.GroupBox7.TabIndex = 61
        Me.GroupBox7.TabStop = false
        Me.GroupBox7.Text = "Historian Tag Filter"
        '
        'txtFilter
        '
        Me.txtFilter.Location = New System.Drawing.Point(7, 27)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(187, 20)
        Me.txtFilter.TabIndex = 0
        '
        'chkSameDestinationName
        '
        Me.chkSameDestinationName.AutoSize = true
        Me.chkSameDestinationName.Location = New System.Drawing.Point(345, 94)
        Me.chkSameDestinationName.Name = "chkSameDestinationName"
        Me.chkSameDestinationName.Size = New System.Drawing.Size(174, 17)
        Me.chkSameDestinationName.TabIndex = 62
        Me.chkSameDestinationName.Text = "Destination Tag  = Same Name"
        Me.chkSameDestinationName.UseVisualStyleBackColor = true
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 706)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(906, 22)
        Me.StatusStrip1.TabIndex = 55
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(38, 17)
        Me.ToolStripStatusLabel1.Text = "status"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(906, 728)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.chkBulkWrite)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.lvwTags)
        Me.Controls.Add(Me.lblStartDate)
        Me.Controls.Add(Me.dtStartDate)
        Me.Controls.Add(Me.lblEndDate)
        Me.Controls.Add(Me.dtEndDate)
        Me.Controls.Add(Me.btnStartExport)
        Me.Controls.Add(Me.lblResults)
        Me.Controls.Add(Me.ListBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "GE Historian Migration and Export"
        Me.lvMenu.ResumeLayout(false)
        Me.TabControl1.ResumeLayout(false)
        Me.TabPage1.ResumeLayout(false)
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.TabPage2.ResumeLayout(false)
        Me.TabPage2.PerformLayout
        Me.TabPage3.ResumeLayout(false)
        Me.TabPage3.PerformLayout
        Me.TabPage4.ResumeLayout(false)
        Me.GroupBox3.ResumeLayout(false)
        Me.TabPage5.ResumeLayout(false)
        Me.TabPage5.PerformLayout
        Me.TabPage6.ResumeLayout(false)
        Me.GroupBox6.ResumeLayout(false)
        Me.GroupBox5.ResumeLayout(false)
        Me.GroupBox4.ResumeLayout(false)
        Me.GroupBox4.PerformLayout
        Me.TabPage7.ResumeLayout(false)
        Me.GroupBox7.ResumeLayout(false)
        Me.GroupBox7.PerformLayout
        Me.StatusStrip1.ResumeLayout(false)
        Me.StatusStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents lblResults As System.Windows.Forms.Label
    Friend WithEvents btnStartExport As System.Windows.Forms.Button
    Friend WithEvents lblStartDate As System.Windows.Forms.Label
    Friend WithEvents dtStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEndDate As System.Windows.Forms.Label
    Friend WithEvents dtEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lvwTags As System.Windows.Forms.ListView
    Friend WithEvents SourceTagName As System.Windows.Forms.ColumnHeader
    Friend WithEvents DestinationTagName As System.Windows.Forms.ColumnHeader
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtDestinationPassword As TextBox
    Friend WithEvents txtDestinationUsername As TextBox
    Friend WithEvents txtDestinationServerName As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtSourcePassword As TextBox
    Friend WithEvents txtSourceUserName As TextBox
    Friend WithEvents txtSourceServerName As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnGetSourceTags As Button
    Friend WithEvents cmdLoadTagCSV As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label12 As Label
    Friend WithEvents cboDebug As ComboBox
    Friend WithEvents chkLogToFile As CheckBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnReadState As Button
    Friend WithEvents btnCreateOfflineArchives As Button
    Friend WithEvents btnResetCreationState As Button
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents Label4 As Label
    Friend WithEvents txtThreads As TextBox
    Friend WithEvents btnGetDestinationTags As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents rbCSVFile As RadioButton
    Friend WithEvents rbHistorianServer As RadioButton
    Friend WithEvents cmdBrowseFolder As Button
    Friend WithEvents txtDataFolder As TextBox
    Friend WithEvents DataFolderBrowser As FolderBrowserDialog
    Friend WithEvents Label15 As Label
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents Label21 As Label
    Friend WithEvents btnCSVQueryTags As Button
    Friend WithEvents btnCSVLoadCSV As Button
    Friend WithEvents btnCSVSaveCSV As Button
    Friend WithEvents txtCSVFilterQuery As TextBox
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents CheckedListBox1 As CheckedListBox
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents lvMenu As ContextMenuStrip
    Friend WithEvents miSelectAll As ToolStripMenuItem
    Friend WithEvents miClearAll As ToolStripMenuItem
    Friend WithEvents chkBulkWrite As CheckBox
    Friend WithEvents btnTestDestinationHistorian As Button
    Friend WithEvents btnTestSourceHistorian As Button
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents btnGetTags As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents txtFilter As TextBox
    Friend WithEvents chkSameDestinationName As CheckBox
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripProgressBar1 As ToolStripProgressBar
End Class
