﻿Imports System
Imports System.IO
Imports System.Text
Imports System.Threading
Imports System.Timers
Imports System.Text.RegularExpressions
Imports INS_CS_Hist_Helper

Public Class HistorianMigration
    Private _HistHelper As INS_CS_Hist_Helper.UserDefinedFunctions
    Dim pHistorian_Source As New INS_CS_Hist_Helper.UserDefinedFunctions
    Dim pHistorian_Destination As New INS_CS_Hist_Helper.UserDefinedFunctions

    Dim MigrationData As String

    Public Enum DebugType
        DebugOff = True
        DebugOn = False
    End Enum


    Public FPstartDate As String
    Public FPendDate As String
    Public FPtaglist As DataTable

    Dim TPstartDate As String
    Dim TPendDate As String
    Dim TPsourceTag As String
    Dim TPdestinationTag As String


    Dim blnFirstRun As Boolean = True


    'Create Delegate/Event for completion of the Migration
    Public Delegate Sub MigrationHandler(ByVal message As String)
    Public Event MigrationComplete As MigrationHandler

    'Create Delegate/Event for messaging during migration
    Public Delegate Sub MigrationMessageHandler(ByVal message As String)
    Public Event MigrationMessaging As MigrationMessageHandler

    'Create Feedback for Number of Tags Processed
    Public Delegate Sub MigrationTagCountHandler(ByVal message As String)
    Public Event MigrationTagCount As MigrationTagCountHandler

    'Create the Migration Thread
    Dim fullMigrateThread As New System.Threading.Thread(AddressOf StartMigration)
    Dim tagMigrateThread As New System.Threading.Thread(AddressOf StartTagMigration)
    'Dim FileStreamThread As New System.Threading.Thread(AddressOf CSVStream)

    Private _DebugMode As DebugType
    Public Property DebugMode() As DebugType
        Get
            Return _DebugMode
        End Get
        Set(ByVal value As DebugType)
            _DebugMode = value
        End Set
    End Property

    Public Sub New(HistorianSource As proficyHistorianServer, HistorianDestination As proficyHistorianServer, sourceTag As String, destinationTag As String, StartDate As String, EndDate As String, Optional debugMode As DebugType = DebugType.DebugOff)
        Dim blnSourceHistorianConnected As Boolean = False
        Dim blnDestinationHistorianConnected As Boolean = False

        _DebugMode = debugMode

        'Connect to the Source Historian
        If pHistorian_Source.ConnectHistorian(HistorianSource.ServerName, HistorianSource.Username, HistorianSource.Password) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            blnSourceHistorianConnected = True
        Else

        End If

        'Connect to the Source Historian
        If pHistorian_Destination.ConnectHistorian(HistorianDestination.ServerName, HistorianDestination.Username, HistorianDestination.Password) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            blnDestinationHistorianConnected = True
        Else

        End If

        If blnSourceHistorianConnected And blnDestinationHistorianConnected Then

            TPstartDate = StartDate
            TPendDate = EndDate
            TPsourceTag = sourceTag
            TPdestinationTag = destinationTag

            tagMigrateThread.Start()

        End If

    End Sub

    Public Sub New(HistorianSource As proficyHistorianServer, HistorianDestination As proficyHistorianServer, tagList As DataTable, StartDate As String, EndDate As String, Optional debugMode As DebugType = DebugType.DebugOff)
        Dim blnSourceHistorianConnected As Boolean = False
        Dim blnDestinationHistorianConnected As Boolean = False

        _DebugMode = debugMode

        'Connect to the Source Historian
        If pHistorian_Source.ConnectHistorian(HistorianSource.ServerName, HistorianSource.Username, HistorianSource.Password) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            blnSourceHistorianConnected = True
            RaiseEvent MigrationMessaging("++Connected To Source:  " & HistorianSource.ServerName.ToString)
        Else
            RaiseEvent MigrationMessaging("xx NOT Connected To Source:  " & HistorianSource.ServerName.ToString)
        End If

        'Connect to the Destination Historian
        If pHistorian_Destination.ConnectHistorian(HistorianDestination.ServerName, HistorianDestination.Username, HistorianDestination.Password) = Proficy.Historian.UserAPI.ihuErrorCode.OK Then
            blnDestinationHistorianConnected = True
            RaiseEvent MigrationMessaging("++Connected To Source:  " & HistorianDestination.ServerName.ToString)
        Else
            RaiseEvent MigrationMessaging("xx NOT Connected To Source:  " & HistorianDestination.ServerName.ToString)
        End If

        If blnSourceHistorianConnected And blnDestinationHistorianConnected Then

            FPtaglist = tagList
            FPstartDate = StartDate
            FPendDate = EndDate

            fullMigrateThread.Start()

        End If

    End Sub

    Private Sub StartMigration()
        Try
            Dim DT As DataTable = Nothing
            Dim ErrorMessage As String = ""
            Dim sourceTag As String = ""
            Dim destinationTag As String = ""

            Dim dtStartDate As String = FPstartDate
            Dim dtEndDate As String = FPendDate
            Dim lvwTags As DataTable = FPtaglist

            Dim startTime As DateTime = Convert.ToDateTime(dtStartDate)
            Dim endTime As DateTime = Convert.ToDateTime(dtEndDate)
            Dim MigratedCount As Integer = 0

            '---new
            dim results as new ArrayList

            RaiseEvent MigrationMessaging("------------------ Starting Data Migration from " & startTime.ToString & " - " & endTime.ToString)

            'Check Offline Archives Bit
            Dim archiveName As String = ""
            Dim archivesBit As String = ""

            If pHistorian_Destination.ReadDefaultDatastoreCreateOfflineArchives(archiveName,archivesBit, ErrorMessage) = 1 then
                RaiseEvent MigrationMessaging("Destination Server: CreateOfflineArchives already enabled")
            else
                archivesBit=""
                ErrorMessage=""
                If Not pHistorian_Destination.SetDefaultDatastoreCreateOfflineArchives(True, archivesBit, errorMessage) Then
                    'Throw New Exception(errorMessage)
                    RaiseEvent MigrationMessaging("Destination Server: Error - Unable to Set CreateOfflineArchives Bit")
                Else
                    RaiseEvent MigrationMessaging("Destination Server: CreateOfflineArchives enabled (Set = 1)")
                End If
                
            End If

            'For Each lvi As ListViewItem In lvwTags.Items
            For Each row As DataRow In lvwTags.Rows
                sourceTag = row.Item("SourceTag")
                destinationTag = row.Item("DestinationTag")
                'stop
                If row.Item("Selected").tolower = "x" Then
                    RaiseEvent MigrationMessaging(">>>>> Migrating Tag: " & vbTab & sourceTag & " to Tag: " & vbTab & destinationTag)

                    ' ReadRawDataByTime(System.DateTime StartTime, System.DateTime EndTime, string Tags, out ArrayList results, out string ErrorMessage)
                    If Not pHistorian_Source.ReadRawDataByTime(startTime, endTime, sourceTag, results, ErrorMessage)

                    'If Not pHistorian_Source.ReadRawDataByTime(startTime, endTime, sourceTag, DT, ErrorMessage) Then
                        RaiseEvent MigrationMessaging("** ReadRawDataByTime ERROR: " & ErrorMessage)
                    Else
                        'Logic to Bulk Send Data
                        'stop
                        RaiseEvent MigrationMessaging("Migrating " & results.Count & " rows.")

                        if Form1.chkBulkWrite.Checked then
                            'Use Bulk write approach
                            If Not pHistorian_Destination.WriteBulkHistorianData(results, ErrorMessage) Then
                                RaiseEvent MigrationMessaging("** WriteBulkHistorianData ERROR: " & ErrorMessage)
                            End If
                        Else 
                            'Use Value by Value write approach
                            for each HR as Object in results
                                If pHistorian_Destination.WriteHistorianData(destinationTag, HR.Timestamp, HR.Value, True, ErrorMessage) Then
                                    'Logit("Write Success!")
                                    If _DebugMode = DebugType.DebugOn Then
                                        RaiseEvent MigrationMessaging("Tag: " & destinationTag & vbTab & " @timestamp=" & HR.Timestamp & vbTab & " @Value=" & hr.value)
                                    End If

                                Else
                                    RaiseEvent MigrationMessaging("**ERROR - Data Write Failed on:  " & destinationTag & vbTab & " with timestamp: " & hr.Timestamp)
                                End If
                            Next
                        end if

                        'Logic for each timestamp
                        'For Each dr As DataRow In DT.Rows
                        '    If pHistorian_Destination.WriteHistorianData(destinationTag, dr(2), dr(4), True, ErrorMessage) Then
                        '        'Logit("Write Success!")
                        '        If _DebugMode = DebugType.DebugOn Then
                        '            RaiseEvent MigrationMessaging("Tag: " & destinationTag & vbTab & " @timestamp=" & dr(2) & vbTab & " @Value=" & dr(4))
                        '        End If

                        '    Else
                        '        RaiseEvent MigrationMessaging("**ERROR - Data Write Failed on:  " & destinationTag & vbTab & " with timestamp: " & dr(2))
                        '    End If
                        'Next
                    End If
                End If

                'Reset Archive Bit
                archivesBit=""
                ErrorMessage=""
                If Not pHistorian_Destination.SetDefaultDatastoreCreateOfflineArchives(False, archivesBit, errorMessage) Then
                    'Throw New Exception(errorMessage)
                    RaiseEvent MigrationMessaging("Destination Server: Error - Unable to turn OFF CreateOfflineArchives Bit")
                Else
                    RaiseEvent MigrationMessaging("Destination Server: CreateOfflineArchives disabled (Set = 0)")
                End If

                RaiseEvent MigrationMessaging("Migration Completed: " & sourceTag)
                MigratedCount+=1
                RaiseEvent MigrationTagCount(MigratedCount)
            Next

            RaiseEvent MigrationTagCount("done")
            MigrationCompleted()

        Catch ex As Exception
            RaiseEvent MigrationMessaging("** ERROR: " & ex.Message)
        End Try

    End Sub

    Private Sub StartTagMigration()
        Try
            Dim DT As DataTable = Nothing
            Dim ErrorMessage As String = ""

            Dim dtStartDate As String = TPstartDate
            Dim dtEndDate As String = TPendDate
            Dim sourceTag As String = TPsourceTag
            Dim destinationTag As String = TPdestinationTag

            Dim startTime As DateTime = Convert.ToDateTime(DateAdd(DateInterval.Hour, -4, Convert.ToDateTime(dtStartDate)))
            Dim endTime As DateTime = Convert.ToDateTime(dtEndDate)

            '---new
            dim results as new ArrayList

            RaiseEvent MigrationMessaging("Migrating: " & sourceTag & vbTab & "To: " & destinationTag)

            ' ReadRawDataByTime(System.DateTime StartTime, System.DateTime EndTime, string Tags, out ArrayList results, out string ErrorMessage)
            If not pHistorian_Source.ReadRawDataByTime(startTime, endTime, sourceTag, results,ErrorMessage)
            'If Not pHistorian_Source.ReadRawDataByTime(startTime, endTime, sourceTag, DT, ErrorMessage) Then
                'Throw New Exception(ErrorMessage)
                RaiseEvent MigrationMessaging("** ERROR: " & ErrorMessage)
                stop
            Else
                For Each dr As DataRow In DT.Rows
                    If pHistorian_Destination.WriteHistorianData(destinationTag, dr(2), dr(4), True, ErrorMessage) Then
                        'Logit("Write Success!")
                    Else
                        RaiseEvent MigrationMessaging("**ERROR - Data Write Failed on:  " & destinationTag & vbTab & " with timestamp: " & dr(2))
                    End If
                Next
            End If


            RaiseEvent MigrationMessaging("Migration Completed: " & sourceTag)
            MigrationCompleted()

        Catch ex As Exception
            RaiseEvent MigrationMessaging("** ERROR: " & ex.Message)
        End Try

    End Sub

    Public Sub StopMigration()
        Try



        Catch ex As Exception
        End Try

    End Sub


    Private Sub MigrationCompleted()
        'Send event back to notify that migration is completed
        RaiseEvent MigrationComplete(MigrationData & " is completed")
    End Sub


End Class
