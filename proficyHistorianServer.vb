﻿Public Class proficyHistorianServer
    Private _ServerName As String
    Public Property ServerName() As String
        Get
            Return _ServerName
        End Get
        Set(ByVal value As String)
            _ServerName = value
        End Set
    End Property

    Private _Username As String
    Public Property Username() As String
        Get
            Return _Username
        End Get
        Set(ByVal value As String)
            _Username = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Public Sub New(ServerName As String, Username As String, Password As String)
        _ServerName = ServerName
        _Username = Username
        _Password = Password
    End Sub
End Class
